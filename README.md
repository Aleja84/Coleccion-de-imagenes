# Proyecto de Tesis: Detección de Maquinaria Minera

Este repositorio contiene una colección de imágenes anotadas utilizadas como parte del proyecto de tesis **Desarrollo de una aplicación off-line para el monitoreo automático
del proceso de carga de material minero usando vistas aéreas**. A continuación, se detalla la información relevante sobre la colección y su proceso de creación.

## Descripción de la Colección

La colección de imágenes ha sido creada a partir de videos obtenidos de YouTube y material proporcionado por el **Grupo QAIRA de la minera Antamina**. Las imágenes abarcan diferentes tipos de vista, incluyendo tomas frontales, aéreas y de cámara fija. En total, se han anotado **1572 imágenes** distribuidas en las siguientes clases:

- Camión minero
- Bulldozer
- Excavadora
- Camión
- Auto
- Persona
- Otro

Cada imagen ha sido anotada utilizando la herramienta **LabelImg** (Singh et al., 2019), generando cuadros delimitadores para cada objeto, con coordenadas `xmin`, `ymin`, `xmax` y `ymax`.

## Formato de Anotación

Las anotaciones están en el formato **PASCAL VOC** (Everingham et al., 2010), con un archivo XML por imagen que contiene las etiquetas y coordenadas de los objetos anotados.

## Fuentes de Datos

Los videos utilizados para extraer imágenes provienen de los siguientes canales de YouTube:

- [A Hole Lotta Digging Going On](https://www.youtube.com/watch?v=gvbrsJvft7s)
- [Aerial View Of Coal Mine Santa Lucia](https://www.youtube.com/watch?v=uvlrV-D6gIU)
- [AUSTRALIA’S HEART OF GOLD](https://www.youtube.com/watch?v=Wko-yJXvnZY)
- *(para ver todas las fuentes, consulte la sección de bibliografía en el archivo)*

## Herramientas y Bibliografía

- **Herramienta de anotación**: LabelImg ([Tzutalin, 2015](https://github.com/tzutalin/labelImg))
- **Formato de anotación**: PASCAL VOC ([Everingham et al., 2010](https://doi.org/10.1007/s11263-009-0275-4))

### Referencias

- Everingham, M., et al. (2010). *The pascal visual object classes (VOC) challenge*. International Journal of Computer Vision, 88(2), 303–338.
- Tzutalin. (2015). *LabelImg*. [Repositorio en GitHub](https://github.com/tzutalin/labelImg)

## Contacto

Para consultas relacionadas con el uso de esta colección, por favor, contáctame.
